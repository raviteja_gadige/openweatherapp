//
//  WeatherManager.swift
//  OpenWeather
//
//  Created by G RaviTeja on 25/03/23.
//

import Foundation
import CoreLocation

class WeatherManager: NSObject {
    
    let apiKey = "214dfa83e8b1c7215bfdeb2b8baef23e"
    
    func getCurrentWeather(latitude: CLLocationDegrees, longitude: CLLocationDegrees) async throws -> ResponseBody {
        
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=\(latitude)&lon=\(longitude)&appid=\(apiKey)&units=metric") else { fatalError("Missing URL") }
        
        
        let urlRequest = URLRequest(url: url)
        
        let (data, response) = try await URLSession.shared.data(for: urlRequest)
        
        guard (response as? HTTPURLResponse)?.statusCode == 200 else { fatalError("Error while fetching data") }
        
        let decodedData = try JSONDecoder().decode(ResponseBody.self, from: data)
        
        return decodedData
    }
    
    func getSelectedCityWeather(cityName: String)  async throws -> ResponseBody {
        
        let cityname = cityName.replacingOccurrences(of: " ", with: "%20")
        
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?q=\(cityname)&appid=\(apiKey)") else { fatalError("Missing URL") }
        
        
        let urlRequest = URLRequest(url: url)
        
        let (data, response) = try await URLSession.shared.data(for: urlRequest)
        
        guard (response as? HTTPURLResponse)?.statusCode == 200 else { fatalError("Error while fetching data") }
        
        let decodedData = try JSONDecoder().decode(ResponseBody.self, from: data)
        
        return decodedData
    }
    
}

