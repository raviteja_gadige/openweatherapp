//
//  ViewController.swift
//  OpenWeather
//
//  Created by G RaviTeja on 23/03/23.
//

import UIKit
import CoreLocation
class ViewController: UIViewController  {
    
    var weatherManager = WeatherManager()
    
    var weather: ResponseBody?
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temparatureLabel: UILabel!
    @IBOutlet weak var weatherTypeLabel: UILabel!
    @IBOutlet weak var latiLabel: UILabel!
    @IBOutlet weak var longLabel: UILabel!
    @IBOutlet weak var locationTextfield: UITextField!
    
    @IBOutlet weak var currentLocationButton: UIButton!
    override func viewDidLoad()   {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
    }
    
    @IBAction func getCurrentOrSelectedLocation(_ sender: Any) {
        
        if locationTextfield.text == "" {
            retriveLocationDetails()
        } else {
            Task {
                weather = try await weatherManager.getSelectedCityWeather(cityName:locationTextfield.text ?? "")
                self.cityLabel.text = weather?.name
                self.temparatureLabel.text = weather?.main.temp.roundDouble()
                self.weatherTypeLabel.text = weather?.wind.speed.roundDouble()
                self.latiLabel.text = weather?.coord.lat.roundDouble()
                self.longLabel.text = weather?.coord.lon.roundDouble()
                
            }
        }
    }
    
    
    func retriveLocationDetails()
    {
        let status = CLLocationManager.authorizationStatus()
        
        if(status == .denied || status == .restricted || !CLLocationManager.locationServicesEnabled()){
            return
        }
        
        if(status == .notDetermined){
            locationManager.requestWhenInUseAuthorization()
            return
        }
        
        locationManager.requestLocation()
        
    }
    
}

extension ViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .authorizedAlways:
            print("user allow app to get location data when app is active or in background")
        case .authorizedWhenInUse:
            print("user allow app to get location data only when app is active")
        case .denied:
            print("user tap 'disallow' on the permission dialog, cant get location data")
        case .restricted:
            print("parental control setting disallow location data")
        case .notDetermined:
            print("the location permission dialog haven't shown before, user haven't tap allow/disallow")
        default:
            print("the location permission dialog haven't shown before, user haven't tap allow/disallow")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])  {
        
        // the last element is the most recent location
        if let location = locations.last {
            
            Task{
                weather = try await weatherManager.getCurrentWeather(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                
                self.cityLabel.text = weather?.name
                self.temparatureLabel.text = weather?.main.temp.roundDouble()
                self.weatherTypeLabel.text = weather?.wind.speed.roundDouble()
                self.latiLabel.text = weather?.coord.lat.roundDouble()
                self.longLabel.text = weather?.coord.lon.roundDouble()
                
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error ",error)
        
    }
    
}

extension Double {
    func roundDouble() -> String {
        return String(format: "%.0f", self)
    }
}

